<?php


namespace ShopExpress\RussianPost;


/**
 * Class BlankHelper
 * @package ShopExpress\RussianPost
 */
class BlankHelper
{
    /**
     * @var array
     */
    public static $form = ['1' => 0, '2' => 1, '1f' => 0, '2f' => 1, '3' => 1, '4' => 1];
    /**
     * @var array
     */
    public static $words = [
        '0' => ['', 'десять', '', ''],
        '1' => ['один', 'одиннадцать', '', 'сто'],
        '2' => ['два', 'двенадцать', 'двадцать', 'двести'],
        '1f' => ['одна', '', '', ''],
        '2f' => ['две', '', '', ''],
        '3' => ['три', 'тринадцать', 'тридцать', 'триста'],
        '4' => ['четыре', 'четырнадцать', 'сорок', 'четыреста'],
        '5' => ['пять', 'пятнадцать', 'пятьдесят', 'пятьсот'],
        '6' => ['шесть', 'шестнадцать', 'шестьдесят', 'шестьсот'],
        '7' => ['семь', 'семнадцать', 'семьдесят', 'семьсот'],
        '8' => ['восемь', 'восемнадцать', 'восемьдесят', 'восемьсот'],
        '9' => ['девять', 'девятнадцать', 'девяносто', 'девятьсот'],
    ];
    /**
     * @var array
     */
    public static $rank = [
        0 => ['рубль', 'рубля', 'рублей'],
        1 => ['тысяча', 'тысячи', 'тысяч'],
        2 => ['миллион', 'миллиона', 'миллионов'],
        3 => ['миллиард', 'миллиарда', 'миллиардов'],
        'k' => ['копейка', 'копейки', 'копеек'],
    ];

    /**
     * @param $str
     * @param bool $show_kop
     * @param bool $show_rank
     * @param bool $rank_only
     *
     * @return string
     */
    public static function doit($str, $show_kop = true, $show_rank = true, $rank_only = false)
    {
        $rank = self::$rank;
        $rank[0] = '';

        $str = number_format($str, 2, '.', ',');
        $rubkop = explode('.', $str);
        $rub = $rubkop[0];
        $kop = $rubkop[1] ?? '00';
        $rub = (strlen($rub) == 1) ? '0' . $rub : $rub;
        $rub = explode(',', $rub);
        $rub = array_reverse($rub);

        $word = [];
        $word[] = self::pluralForm($rank['k'], $kop);

        foreach ($rub as $key => $value) {
            if ((int)$value > 0 || $key == 0) {
                $str_rub = self::clearDvig($value, $key);

                $str_rank = self::pluralForm($rank[$key], $value);
                $str_rank = str_replace($value, '', $str_rank);
                $word[] = $str_rub . ($str_rank ? ' ' . $str_rank : '');
            }
        }
        if (!$show_rank) {
            unset($word[0]);
        }

        if ($rank_only) {
            $word = [$word[0]];
        }

        $word = array_reverse($word);
        return ucfirst(trim(implode(' ', $word)));
    }

    /**
     * @param $str
     * @param $key
     * @param bool $do_word
     *
     * @return string
     */
    public static function clearDvig($str, $key, $do_word = true)
    {
        $words = self::$words;
        $form = self::$form;

        $rank = '';
        $str = (strlen($str) == 1) ? '0' . $str : $str;
        $dig = str_split($str);
        $dig = array_reverse($dig);

        if (1 == $dig[1]) {
            $num_word = ($do_word) ? $words[$dig[0]][1] : $dig[1] . $dig[0];
        } else {
            if ($key == 1) {
                $rank = 'f';
            }
            if ($dig[0] != 1 && $dig[0] != 2) {
                $rank = '';
            }
            $num_word = ($do_word) ? $words[$dig[1]][2] . ' ' . $words[$dig[0] . $rank][0] : $dig[1] . $dig[0];
            //$key = $form[$dig[0]] ?? false;
        }

        $sotni = (isset($dig[2])) ? (($do_word) ? $words[$dig[2]][3] : $dig[2]) : '';
        if ($sotni && $do_word) {
            $sotni .= ' ';
        }

        return $sotni . $num_word;
    }

    /**
     * @param $str
     * @param $key
     * @param bool $do_word
     * @param bool $show_rank
     *
     * @return array
     */
    public static function dvig($str, $key, $do_word = true, $show_rank = true)
    {
        $words = self::$words;
        $form = self::$form;

        $rank = '';

        $str = (strlen($str) == 1) ? '0' . $str : $str;
        $dig = str_split($str);
        $dig = array_reverse($dig);

        if (1 == $dig[1]) {
            $num_word = ($do_word) ? $words[$dig[0]][1] : $dig[1] . $dig[0];
        } else {
            if ($key == 1) {
                $rank = 'f';
            }
            if ($dig[0] != 1 && $dig[0] != 2) {
                $rank = '';
            }
            $num_word = ($do_word) ? $words[$dig[1]][2] . ' ' . $words[$dig[0] . $rank][0] : $dig[1] . $dig[0];
        }

        $sotni = (isset($dig[2])) ? (($do_word) ? $words[$dig[2]][3] : $dig[2]) : '';
        if ($sotni && $do_word) {
            $sotni .= ' ';
        }

        return [$sotni . $num_word];
    }

    /**
     * @param $str
     *
     * @return false|string
     */
    public static function clearDoit($str)
    {
        $rank = [
            0 => 1,
            1 => 1,
            2 => 1,
            3 => 1,
            'k' => 1,
        ];

        $str = number_format($str, 2, '.', ',');
        $rubkop = explode('.', $str);
        $rub = $rubkop[0];
        $kop = $rubkop[1] ?? '00';
        $rub = (strlen($rub) == 1) ? '0' . $rub : $rub;
        $rub = explode(',', $rub);
        $rub = array_reverse($rub);

        $word = [];
        $word[] = self::pluralForm(self::$rank['k'], $kop);

        foreach ($rub as $key => $value) {
            if ((int)$value > 0 || $key == 0) {
                $str_rub = self::clearDvig($value, $key);
                $str_rank = self::pluralForm($rank[$key], [$value]);
                $str_rank = str_replace($value, '', $str_rank);
                $word[] = $str_rub . $str_rank;
            }
        }

        $word = array_reverse($word);

        return self::mbUcfirst(trim(implode(' ', $word)));
    }

    /**
     * @param $forms
     * @param $n
     *
     * @return mixed
     */
    public static function pluralForm($forms, $n)
    {
        if (!is_array($forms)) {
            return '';
        }
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }

    /**
     * @param $str
     * @param string $encoding
     *
     * @return false|string
     */
    public static function mbUcfirst($str, $encoding = 'UTF-8')
    {
        $str = mb_ereg_replace('^[\ ]+', '', $str);
        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
            mb_substr($str, 1, mb_strlen($str), $encoding);
        return $str;
    }

    /**
     * @param $total
     *
     * @return array
     */
    public static function getRubKop($total): array
    {
        $rub = '0';
        $kop = '00';
        if (is_numeric($total)) {
            $total_array = explode('.', $total);
            $rub = reset($total_array);

            if (!empty($total_array[1])) {
                $kop = $total_array[1];
            } else {
                $total = (float)$rub . '.' . $kop;
            }
        } else {
            $total = 0;
        }

        return [$total, $rub, $kop];
    }

    /**
     * @param $phone
     *
     * @return string|string[]
     */
    public static function preparePhone($phone)
    {
        $arraySearch = ['+', '7', '8'];

        $phone = str_replace('-', '', $phone);
        $phone = str_replace($arraySearch, '', substr($phone, 0, 2)) . substr($phone, 2);

        return $phone;
    }
}
