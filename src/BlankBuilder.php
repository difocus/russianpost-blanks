<?php

namespace ShopExpress\RussianPost;

use Smarty;
use SmartyException;

/**
 * Class BlankFactory
 * @package ShopExpress\RussianPost
 */
class BlankBuilder
{
    public const BLANK_7A = 'blank_7a';
    public const BLANK_7P = 'blank_7p';
    public const BLANK_112EP = 'blank_112ep';
    public const BLANK_116 = 'blank_116';
    public const BLANK_107 = 'blank_107';

    public const MAILING_TYPE_PACKAGE = 'package';
    public const MAILING_TYPE_PARCEL = 'parcel';

    public const SENDER_TYPE_COMPANY = 1;
    public const SENDER_TYPE_INDIVIDUAL = 0;

    /**
     * @var Smarty
     */
    private $smarty;

    /**
     * BlankBuilder constructor.
     *
     * @param string $assetsPath
     * @param string $compileDir
     */
    public function __construct(string $assetsPath = 'assets', string $compileDir = __DIR__ . '/../tmp')
    {
        $this->smarty = new Smarty();
        $this->smarty
            ->setTemplateDir(__DIR__ . '/../templates')
            ->setCompileDir($compileDir);
        $this->smarty->setErrorReporting(E_ALL & ~E_NOTICE);
        $this->smarty->assign('images_dir', $assetsPath);
    }

    /**
     * @param string $name
     *
     * @param bool $openWindowPrint
     *
     * @throws SmartyException
     * @return string
     */
    public function print(string $name, bool $openWindowPrint = false): string
    {
        try {
            $this->smarty->assign('window_print', $openWindowPrint);
            $template = $this->smarty->createTemplate($name . '.tpl');
        } catch (SmartyException $e) {
            throw new \RuntimeException('Invalid blank alias.');
        }

        return $template->fetch();
    }

    /**
     * @param bool $value
     *
     * @return BlankBuilder
     */
    public function setPrintBackground(bool $value): BlankBuilder
    {
        $this->smarty->assign('print_bg', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setMailingType(string $value): BlankBuilder
    {
        $this->smarty->assign('type_mailing', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setTotalDeclared(string $value): BlankBuilder
    {
        [, $declaredRub, $declaredKop] = BlankHelper::getRubKop($value);
        $totalCen = BlankHelper::doit($value, false, false);

        $this->smarty->assign('total_cen', $totalCen);
        $this->smarty->assign('t_declared_kop', $declaredRub . ' (' . $totalCen . ') руб. ' . $declaredKop . ' коп.');
        $this->smarty->assign('total_declared', $declaredRub . ' (' . $totalCen . ') руб. ' . $declaredKop . ' коп.');

        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setTotalImposed(string $value): BlankBuilder
    {
        [, $iposedRub, $imposedKop] = BlankHelper::getRubKop($value);
        $totalCod = BlankHelper::doit($value, false, false);

        $this->smarty->assign('total_imposed', $iposedRub . ' (' . $totalCod . ') руб. ' . $imposedKop . ' коп.');
        $this->smarty->assign('total_cod', $totalCod);
        $this->smarty->assign('imposed_rub', $iposedRub);
        $this->smarty->assign('imposed_kop', $imposedKop);
        $this->smarty->assign('t_imposed', BlankHelper::clearDoit($value));

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return BlankBuilder
     */
    public function setSmsForSender(bool $value): BlankBuilder
    {
        $this->smarty->assign('sms_for_sender', $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return BlankBuilder
     */
    public function setCompanyPhone(string $value): BlankBuilder
    {
        $this->smarty->assign('company_phone', BlankHelper::preparePhone($value));
        return $this;
    }

    /**
     * @param $value
     *
     * @return BlankBuilder
     */
    public function setSender(string $value): BlankBuilder
    {
        $this->smarty->assign('sender', $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return BlankBuilder
     */
    public function setIndex(string $value): BlankBuilder
    {
        $this->smarty->assign('index', $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return BlankBuilder
     */
    public function setFizIndex(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_index', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFromWhom(string $value): BlankBuilder
    {
        $this->smarty->assign('from_whom', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFromWhom2(string $value): BlankBuilder
    {
        $this->smarty->assign('from_whom2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setSenderAddress(string $value): BlankBuilder
    {
        $this->smarty->assign('sender_address', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setSenderAddress2(string $value): BlankBuilder
    {
        $this->smarty->assign('sender_address2', $value);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return BlankBuilder
     */
    public function setSmsForRecepient(bool $value): BlankBuilder
    {
        $this->smarty->assign('sms_for_recepient', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setRecipientPhone(string $value): BlankBuilder
    {
        $this->smarty->assign('recipient_phone', BlankHelper::preparePhone($value));
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFromIndex(string $value): BlankBuilder
    {
        $this->smarty->assign('from_index', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setWhom(string $value): BlankBuilder
    {
        $this->smarty->assign('whom', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setWhom2(string $value): BlankBuilder
    {
        $this->smarty->assign('whom2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizFio(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_fio', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizFio2(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_fio2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setWhere(string $value): BlankBuilder
    {
        $this->smarty->assign('where', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setWhere2(string $value): BlankBuilder
    {
        $this->smarty->assign('where2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizAddress(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_address', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizAddress2(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_address2', $value);
        return $this;
    }

    /**
     * @param $amount
     * @param $price
     * @param string $product
     *
     * @return BlankBuilder
     */
    public function addProduct($amount, $price, string $product): BlankBuilder
    {
        if (!$products = $this->smarty->getTemplateVars('products')) {
            $products = [];
        }

        $products[] = compact('amount', 'price', 'product');

        $this->smarty->assign('products', $products);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setText(string $value): BlankBuilder
    {
        $this->smarty->assign('text', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setText2(string $value): BlankBuilder
    {
        $this->smarty->assign('text2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setInn(string $value): BlankBuilder
    {
        $this->smarty->assign('inn', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setKor(string $value): BlankBuilder
    {
        $this->smarty->assign('kor', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setRas(string $value): BlankBuilder
    {
        $this->smarty->assign('ras', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setBank(string $value): BlankBuilder
    {
        $this->smarty->assign('bank', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setBik(string $value): BlankBuilder
    {
        $this->smarty->assign('bik', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDoc(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDocSerial(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc_serial', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDocNumber(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc_number', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDocDate(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc_date', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDocDate2(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc_date2', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return BlankBuilder
     */
    public function setFizDocCreator(string $value): BlankBuilder
    {
        $this->smarty->assign('fiz_doc_creator', $value);
        return $this;
    }

}