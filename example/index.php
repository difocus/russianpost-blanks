<?php

if (!file_exists(__DIR__ . '/.env')) {
    exit('.env file does not exists.');
}

$config = parse_ini_file(__DIR__ . '/.env');

require 'vendor/autoload.php';

use ShopExpress\RussianPost\BlankBuilder;

$blankBuilder = new BlankBuilder($config['BASE_DIR']);
$blankBuilder->setPrintBackground(true)
    ->setMailingType(BlankBuilder::MAILING_TYPE_PACKAGE)
    ->setTotalDeclared('1')
    ->setTotalImposed('2')
    ->setSmsForSender(true)
    ->setCompanyPhone('4')
    ->setSender('5')
    ->setIndex('6')
    ->setFizIndex('7')
    ->setFromWhom('8')
    ->setFromWhom2('8')
    ->setSenderAddress('10')
    ->setSenderAddress2('11')
    ->setSmsForRecepient(true)
    ->setRecipientPhone('13')
    ->setFromIndex('14')
    ->setWhom('15')
    ->setWhom2('16')
    ->setFizFio('17')
    ->setFizFio2('18');

$blankBuilder->setWhere('19')
    ->setWhere2('20')
    ->setFizAddress('21')
    ->setFizAddress2('22')
    ->setText('23')
    ->setText2('24')
    ->setInn('12345')
    ->setKor('25')
    ->setRas('26')
    ->setBik('27')
    ->setFizDoc('28')
    ->setFizDocSerial('29')
    ->setFizDocNumber('30')
    ->setFizDocDate('31')
    ->setFizDocDate2('32')
    ->setFizDocCreator('33');

echo $blankBuilder->print(BlankBuilder::BLANK_7P);