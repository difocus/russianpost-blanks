<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {literal}
        <style type="text/css" media="screen,print">

            body, p, div, td {
                color: #000000;
                font: 12px Arial;
            }

            body {
                padding: 0;
                margin: 0;
            }

            a, a:link, a:visited, a:hover, a:active {
                color: #000000;
                text-decoration: underline;
            }

            a:hover {
                text-decoration: none;
            }
        </style>
    {/literal}
</head>

<body style="width: 210mm; height: 293mm;" {if $window_print}onload="print();"{/if}>
<div style="top: {$addons.rus_russianpost.112_top}mm; left: {$addons.rus_russianpost.112_left}mm; width: {$addons.rus_russianpost.112_list_width}mm; height: {$addons.rus_russianpost.112_list_height}mm; position: relative;">
    {if $print_bg}
        <img style="width: 210mm; height: 293mm;" src="{$images_dir}/blank_112ep.jpg"/>
    {/if}

    {if !empty($total_imposed) && $total_imposed}
        <span style="position: absolute; height: 5mm; width: 5mm; top: 70mm; left: 17mm; text-align: center; font: 30pt 'Arial';">&#xD7;</span>
        <span style="position: absolute; height: 5mm; width: 15mm; top: 66.5mm; left: 19mm; text-align: center; font: 11pt 'Arial';">{$imposed_rub}</span>
        <span style="position: absolute; height: 5mm; width: 7mm; top: 66.5mm; left: 42mm; text-align: center; font: 11pt 'Arial';">{$imposed_kop}</span>
        <span style="position: absolute; height: 5mm; width: 125mm; top: 60mm; left: 60mm; text-align: center; font: 10pt 'Arial';">{$t_imposed}</span>
    {/if}

    {if $sms_for_sender}
        <div style="position: absolute; height: 4mm; width: 35mm; top: 71mm; left: 151mm; font: 11pt 'Arial'; margin:0;">
            <span style="position: absolute; left: 0mm;">{$company_phone.0}</span>
            <span style="position: absolute; left: 4mm;">{$company_phone.1}</span>
            <span style="position: absolute; left: 7mm;">{$company_phone.2}</span>
            <span style="position: absolute; left: 11mm;">{$company_phone.3}</span>
            <span style="position: absolute; left: 15mm;">{$company_phone.4}</span>
            <span style="position: absolute; left: 18mm;">{$company_phone.5}</span>
            <span style="position: absolute; left: 22mm;">{$company_phone.6}</span>
            <span style="position: absolute; left: 26mm;">{$company_phone.7}</span>
            <span style="position: absolute; left: 29mm;">{$company_phone.8}</span>
            <span style="position: absolute; left: 32mm;">{$company_phone.9}</span>
        </div>
    {/if}

    {if $sms_for_recepient}
        <div style="position: absolute; height: 4mm; width: 35mm; top: 77mm; left: 151mm; font: 11pt 'Arial'; margin:0;">
            <span style="position: absolute; left: 0mm;">{$recipient_phone.0}</span>
            <span style="position: absolute; left: 4mm;">{$recipient_phone.1}</span>
            <span style="position: absolute; left: 7mm;">{$recipient_phone.2}</span>
            <span style="position: absolute; left: 11mm;">{$recipient_phone.3}</span>
            <span style="position: absolute; left: 15mm;">{$recipient_phone.4}</span>
            <span style="position: absolute; left: 18mm;">{$recipient_phone.5}</span>
            <span style="position: absolute; left: 22mm;">{$recipient_phone.6}</span>
            <span style="position: absolute; left: 26mm;">{$recipient_phone.7}</span>
            <span style="position: absolute; left: 29mm;">{$recipient_phone.8}</span>
            <span style="position: absolute; left: 32mm;">{$recipient_phone.9}</span>
        </div>
        <span style="position: absolute; height: 5mm; width: 4mm; top: 75mm; left: 59mm; text-align: center; font: 15pt 'Arial';">&#xD7;</span>
    {/if}

    <span style="position: absolute; height: 5mm; width: 160mm; top: 83mm; left: 27mm; font: 11pt 'Arial';">{if $sender}{$whom} {$whom2}{else}{$fiz_fio} {$fiz_fio2}{/if}</span>
    <span style="position: absolute; height: 13mm; width: 160mm; top: 89mm; left: 27mm; font: 11pt 'Arial'; line-height: 15pt;">{if $sender}{$where} {$where2}{else}{$fiz_address} {$fiz_address2}{/if}</span>

    <span style="position: absolute; height: 5mm; width: 25mm; top: 96mm; left: 160mm; font: 11pt 'Arial'; letter-spacing: 7.6pt;">
            {if $sender}
                <span style="position: absolute; left: 0mm;">{$index.0}</span>
                <span style="position: absolute; left: 5mm;">{$index.1}</span>
                <span style="position: absolute; left: 9mm;">{$index.2}</span>
                <span style="position: absolute; left: 14mm;">{$index.3}</span>
                <span style="position: absolute; left: 19mm;">{$index.4}</span>
                <span style="position: absolute; left: 23mm;">{$index.5}</span>


{else}


                <span style="position: absolute; left: 0mm;">{$fiz_index.0}</span>
                <span style="position: absolute; left: 5mm;">{$fiz_index.1}</span>
                <span style="position: absolute; left: 9mm;">{$fiz_index.2}</span>
                <span style="position: absolute; left: 14mm;">{$fiz_index.3}</span>
                <span style="position: absolute; left: 19mm;">{$fiz_index.4}</span>
                <span style="position: absolute; left: 23mm;">{$fiz_index.5}</span>
            {/if}
        </span>

    <div style="position: absolute; height: 4.5mm; width: 55mm; top: 104mm; left: 39mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$text.0}</span>
        <span style="position: absolute; left: 5mm;">{$text.1}</span>
        <span style="position: absolute; left: 9mm;">{$text.2}</span>
        <span style="position: absolute; left: 13mm;">{$text.3}</span>
        <span style="position: absolute; left: 17mm;">{$text.4}</span>
        <span style="position: absolute; left: 22mm;">{$text.5}</span>
        <span style="position: absolute; left: 26mm;">{$text.6}</span>
        <span style="position: absolute; left: 30mm;">{$text.7}</span>
        <span style="position: absolute; left: 34mm;">{$text.8}</span>
        <span style="position: absolute; left: 38mm;">{$text.9}</span>
        <span style="position: absolute; left: 43mm;">{$text.10}</span>
        <span style="position: absolute; left: 47mm;">{$text.11}</span>
        <span style="position: absolute; left: 52mm;">{$text.12}</span>
        <span style="position: absolute; left: 56mm;">{$text.13}</span>
        <span style="position: absolute; left: 60mm;">{$text.14}</span>
        <span style="position: absolute; left: 64mm;">{$text.15}</span>
        <span style="position: absolute; left: 69mm;">{$text.16}</span>
        <span style="position: absolute; left: 72mm;">{$text.17}</span>
        <span style="position: absolute; left: 77mm;">{$text.18}</span>
        <span style="position: absolute; left: 81mm;">{$text.19}</span>
        <span style="position: absolute; left: 85mm;">{$text.20}</span>
        <span style="position: absolute; left: 89mm;">{$text.21}</span>
        <span style="position: absolute; left: 94mm;">{$text.22}</span>
        <span style="position: absolute; left: 98mm;">{$text.23}</span>
        <span style="position: absolute; left: 102mm;">{$text.24}</span>
        <span style="position: absolute; left: 106mm;">{$text.25}</span>
        <span style="position: absolute; left: 110mm;">{$text.26}</span>
        <span style="position: absolute; left: 115mm;">{$text.27}</span>
        <span style="position: absolute; left: 119mm;">{$text.28}</span>
        <span style="position: absolute; left: 123mm;">{$text.29}</span>
        <span style="position: absolute; left: 127mm;">{$text.30}</span>
        <span style="position: absolute; left: 132mm;">{$text.31}</span>
        <span style="position: absolute; left: 136mm;">{$text.32}</span>
        <span style="position: absolute; left: 140mm;">{$text.33}</span>
        <span style="position: absolute; left: 144mm;">{$text.34}</span>
    </div>

    <div style="position: absolute; height: 4.5mm; width: 55mm; top: 111mm; left: 39mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$text2.0}</span>
        <span style="position: absolute; left: 5mm;">{$text2.1}</span>
        <span style="position: absolute; left: 9mm;">{$text2.2}</span>
        <span style="position: absolute; left: 13mm;">{$text2.3}</span>
        <span style="position: absolute; left: 17mm;">{$text2.4}</span>
        <span style="position: absolute; left: 22mm;">{$text2.5}</span>
        <span style="position: absolute; left: 26mm;">{$text2.6}</span>
        <span style="position: absolute; left: 30mm;">{$text2.7}</span>
        <span style="position: absolute; left: 34mm;">{$text2.8}</span>
        <span style="position: absolute; left: 38mm;">{$text2.9}</span>
        <span style="position: absolute; left: 43mm;">{$text2.10}</span>
        <span style="position: absolute; left: 47mm;">{$text2.11}</span>
        <span style="position: absolute; left: 52mm;">{$text2.12}</span>
        <span style="position: absolute; left: 56mm;">{$text2.13}</span>
        <span style="position: absolute; left: 60mm;">{$text2.14}</span>
        <span style="position: absolute; left: 64mm;">{$text2.15}</span>
        <span style="position: absolute; left: 69mm;">{$text2.16}</span>
        <span style="position: absolute; left: 72mm;">{$text2.17}</span>
        <span style="position: absolute; left: 77mm;">{$text2.18}</span>
        <span style="position: absolute; left: 81mm;">{$text2.19}</span>
        <span style="position: absolute; left: 85mm;">{$text2.20}</span>
        <span style="position: absolute; left: 89mm;">{$text2.21}</span>
        <span style="position: absolute; left: 94mm;">{$text2.22}</span>
        <span style="position: absolute; left: 98mm;">{$text2.23}</span>
        <span style="position: absolute; left: 102mm;">{$text2.24}</span>
        <span style="position: absolute; left: 106mm;">{$text2.25}</span>
        <span style="position: absolute; left: 110mm;">{$text2.26}</span>
        <span style="position: absolute; left: 115mm;">{$text2.27}</span>
        <span style="position: absolute; left: 119mm;">{$text2.28}</span>
        <span style="position: absolute; left: 123mm;">{$text2.29}</span>
        <span style="position: absolute; left: 127mm;">{$text2.30}</span>
        <span style="position: absolute; left: 132mm;">{$text2.31}</span>
        <span style="position: absolute; left: 136mm;">{$text2.32}</span>
        <span style="position: absolute; left: 140mm;">{$text2.33}</span>
        <span style="position: absolute; left: 144mm;">{$text2.34}</span>
    </div>

    <div style="position: absolute; height: 5mm; width: 50mm; top: 121.5mm; left: 27mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$inn.0}</span>
        <span style="position: absolute; left: 5mm;">{$inn.1}</span>
        <span style="position: absolute; left: 9mm;">{$inn.2}</span>
        <span style="position: absolute; left: 13mm;">{$inn.3}</span>
        <span style="position: absolute; left: 18mm;">{$inn.4}</span>
        <span style="position: absolute; left: 22mm;">{$inn.5}</span>
        <span style="position: absolute; left: 26mm;">{$inn.6}</span>
        <span style="position: absolute; left: 30mm;">{$inn.7}</span>
        <span style="position: absolute; left: 35mm;">{$inn.8}</span>
        <span style="position: absolute; left: 39mm;">{$inn.9}</span>
        <span style="position: absolute; left: 43mm;">{$inn.10}</span>
        <span style="position: absolute; left: 48mm;">{$inn.11}</span>
    </div>
    <div style="position: absolute; height: 5mm; width: 85mm; top: 121.5mm; left: 102.5mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$kor.0}</span>
        <span style="position: absolute; left: 4mm;">{$kor.1}</span>
        <span style="position: absolute; left: 9mm;">{$kor.2}</span>
        <span style="position: absolute; left: 13mm;">{$kor.3}</span>
        <span style="position: absolute; left: 18mm;">{$kor.4}</span>
        <span style="position: absolute; left: 22mm;">{$kor.5}</span>
        <span style="position: absolute; left: 26mm;">{$kor.6}</span>
        <span style="position: absolute; left: 30mm;">{$kor.7}</span>
        <span style="position: absolute; left: 34mm;">{$kor.8}</span>
        <span style="position: absolute; left: 38mm;">{$kor.9}</span>
        <span style="position: absolute; left: 43mm;">{$kor.10}</span>
        <span style="position: absolute; left: 47mm;">{$kor.11}</span>
        <span style="position: absolute; left: 51mm;">{$kor.12}</span>
        <span style="position: absolute; left: 55mm;">{$kor.13}</span>
        <span style="position: absolute; left: 60mm;">{$kor.14}</span>
        <span style="position: absolute; left: 64mm;">{$kor.15}</span>
        <span style="position: absolute; left: 68mm;">{$kor.16}</span>
        <span style="position: absolute; left: 72mm;">{$kor.17}</span>
        <span style="position: absolute; left: 77mm;">{$kor.18}</span>
        <span style="position: absolute; left: 81mm;">{$kor.19}</span>
    </div>
    <span style="position: absolute; height: 5mm; width: 130mm; top: 127mm; left: 55mm; font: 11pt 'Arial';">{$bank}</span>
    <div style="position: absolute; height: 5mm; width: 85mm; top: 133mm; left: 34mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$ras.0}</span>
        <span style="position: absolute; left: 4mm;">{$ras.1}</span>
        <span style="position: absolute; left: 8mm;">{$ras.2}</span>
        <span style="position: absolute; left: 13mm;">{$ras.3}</span>
        <span style="position: absolute; left: 17mm;">{$ras.4}</span>
        <span style="position: absolute; left: 21mm;">{$ras.5}</span>
        <span style="position: absolute; left: 26mm;">{$ras.6}</span>
        <span style="position: absolute; left: 30mm;">{$ras.7}</span>
        <span style="position: absolute; left: 34mm;">{$ras.8}</span>
        <span style="position: absolute; left: 38mm;">{$ras.9}</span>
        <span style="position: absolute; left: 43mm;">{$ras.10}</span>
        <span style="position: absolute; left: 47mm;">{$ras.11}</span>
        <span style="position: absolute; left: 51mm;">{$ras.12}</span>
        <span style="position: absolute; left: 55mm;">{$ras.13}</span>
        <span style="position: absolute; left: 60mm;">{$ras.14}</span>
        <span style="position: absolute; left: 64mm;">{$ras.15}</span>
        <span style="position: absolute; left: 68mm;">{$ras.16}</span>
        <span style="position: absolute; left: 73mm;">{$ras.17}</span>
        <span style="position: absolute; left: 77mm;">{$ras.18}</span>
        <span style="position: absolute; left: 81mm;">{$ras.19}</span>
    </div>
    <div style="position: absolute; height: 5mm; width: 38mm; top: 133mm; left: 149mm; font: 11pt 'Arial'; margin: 0;">
        <span style="position: absolute; left: 0mm;">{$bik.0}</span>
        <span style="position: absolute; left: 4mm;">{$bik.1}</span>
        <span style="position: absolute; left: 9mm;">{$bik.2}</span>
        <span style="position: absolute; left: 13mm;">{$bik.3}</span>
        <span style="position: absolute; left: 17mm;">{$bik.4}</span>
        <span style="position: absolute; left: 22mm;">{$bik.5}</span>
        <span style="position: absolute; left: 26mm;">{$bik.6}</span>
        <span style="position: absolute; left: 30mm;">{$bik.7}</span>
        <span style="position: absolute; left: 34mm;">{$bik.8}</span>
    </div>

    <span style="position: absolute; height: 5mm; width: 150mm; top: 139mm; left: 35mm; font: 11pt 'Arial';">{$from_whom} {$from_whom2}</span>
    <span style="position: absolute; height: 10mm; width: 170mm; top: 145.5mm; left: 17mm; font: 11pt 'Arial'; text-indent: 35mm; line-height: 14pt;">{$sender_address} {$sender_address2}</span>

    <span style="position: absolute; height: 4mm; width: 23mm; top: 151mm; left: 160mm; font: 11pt 'Arial'; letter-spacing: 7.6pt;">
            <span style="position: absolute; left: 0mm;">{$from_index.0}</span>
            <span style="position: absolute; left: 5mm;">{$from_index.1}</span>
            <span style="position: absolute; left: 10mm;">{$from_index.2}</span>
            <span style="position: absolute; left: 14mm;">{$from_index.3}</span>
            <span style="position: absolute; left: 19mm;">{$from_index.4}</span>
            <span style="position: absolute; left: 23mm;">{$from_index.5}</span>
        </span>
</div>
</body>
</html>
